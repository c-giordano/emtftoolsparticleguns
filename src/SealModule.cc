#include "FWCore/Framework/interface/MakerMacros.h"

#include "EMTFTools/ParticleGuns/interface/FlatRandomPtGunProducer2.h"
#include "EMTFTools/ParticleGuns/interface/FlatRandomLLPGunProducer2.h"

using edm::FlatRandomPtGunProducer2;
using edm::FlatRandomLLPGunProducer2;
DEFINE_FWK_MODULE(FlatRandomPtGunProducer2);
DEFINE_FWK_MODULE(FlatRandomLLPGunProducer2);
